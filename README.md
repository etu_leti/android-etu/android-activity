# Android-Activity

#### 1. Создайте логирование используя приложение из предыдущего урока для каждого метода жизненного цикла activity, проверьте, что методы вызываются в нужной последовательности, просмотрите логи.

*Так как нас не интересуют не все логи, а только логи относящиеся к методам жизненного цикла activity. Поэтому отфильтруем логи с помощью поиска по слову "called"*

**Запустил программу:**

    2024-02-26 23:03:35.428  2975-3026  ActivityManager         system_server                        W  setHasOverlayUi called on unknown pid: 17740
    2024-02-26 23:03:35.801 18865-18865 Lifecycle               com.etu.mybusinesscard               D  onCreate() called
    2024-02-26 23:03:35.820 18865-18865 Lifecycle               com.etu.mybusinesscard               D  onStart() called
    2024-02-26 23:03:35.821 18865-18865 Lifecycle               com.etu.mybusinesscard               D  onResume() called
    2024-02-26 23:03:47.220  3867-3867  View                    com.android.systemui                 W  requestLayout() improperly called by android.widget.ProgressBar{22e0b9b V.ED..... ......ID 0,0-84,42 #7f0b0973 

**Обоснование результата:**

В логах отображаются последовательно методы onCreate(), onStart(), onResume(), что свидетельствует об успешном создании и отображении активности:
- onCreate() вызывается при инициализации активности.
- onStart() вызывается, когда активность становится видимой для пользователя.
- onResume() вызывается, когда активность получает фокус и становится полностью доступной для взаимодействия.

#### 2. Включите режим поворота экрана, осуществите поворот экрана, просмотрите логи. 

**Повернул экран:**

    2024-02-27 01:07:53.109  3867-4011  OneHandedD...aOrganizer com.android.systemui                 D  onRotateDisplay: called!!!
    2024-02-27 01:07:53.151 15935-15935 Lifecycle               com.etu.mybusinesscard               D  onPause() called
    2024-02-27 01:07:53.154 15935-15935 Lifecycle               com.etu.mybusinesscard               D  onStop() called
    2024-02-27 01:07:53.163 15935-15935 Lifecycle               com.etu.mybusinesscard               D  onDestroy() called
    2024-02-27 01:07:53.181  1700-1700  DMSService              ven...sp.hardware.dmssp@2.0-service  I  getSelectedTuningDevice called with port=0.
    2024-02-27 01:07:53.185 15935-15935 Lifecycle               com.etu.mybusinesscard               D  onCreate() called
    2024-02-27 01:07:53.199 15935-15935 Lifecycle               com.etu.mybusinesscard               D  onStart() called
    2024-02-27 01:07:53.202 15935-15935 Lifecycle               com.etu.mybusinesscard               D  onResume() called

**Обоснование результата:**

После поворота экрана:
- onPause() вызывается, когда активность перестает быть видимой для пользователя.
- onStop() вызывается, когда активность больше не отображается на экране.
- onDestroy() вызывается, когда активность уничтожается и заканчивает свою работу.
- после этого происходит пересоздание активности и отображаются методы onCreate(), onStart(), onResume().

#### 3. Сверните приложение, просмотрите логи. 

**Свернул приложение:**

    2024-02-27 01:18:18.779  4210-4210  View                    com.android.launcher                 W  requestLayout() improperly called by android.widget.LinearLayout{e62b6e1 VFE...C.. ......ID 0,3-1072,122 #7f0a00a2 app:id/animated_hint_layout viewInfo = } during layout: running second layout pass
    2024-02-27 01:18:19.775  4210-4210  View                    com.android.launcher                 W  requestLayout() improperly called by android.widget.LinearLayout{e62b6e1 VFE...C.. ......ID 0,3-1072,122 #7f0a00a2 app:id/animated_hint_layout viewInfo = } during layout: running second layout pass
    2024-02-27 01:18:19.804 15935-15935 Lifecycle               com.etu.mybusinesscard               D  onPause() called
    2024-02-27 01:18:19.837 15935-15935 Lifecycle               com.etu.mybusinesscard               D  onStop() called

**Обоснование результата:**

После того, как приложение было свернуто:
- onPause() вызывается, когда активность перестает быть видимой для пользователя.
- onStop() вызывается, когда активность больше не отображается на экране.

#### 4. Напишите в методе onCreate() вызов метода finish(), просмотрите логи. 

**Вызвал метод finish() в onCreate():**

    2024-02-27 01:22:14.844  2975-3026  ActivityManager         system_server                        W  setHasOverlayUi called on unknown pid: 15935
    2024-02-27 01:22:15.335  1008-1008  Lifecycle               com.etu.mybusinesscard               D  onCreate() called
    2024-02-27 01:22:15.808  1008-1008  Lifecycle               com.etu.mybusinesscard               D  onDestroy() called

**Обоснование результата:**

 
Метод finish() вызывается в методе onCreate(). Это приводит к тому, что активность уничтожается до того, как она успевает отобразиться на экране:
- Метод onDestroy() завершает жизненный цикл активности.